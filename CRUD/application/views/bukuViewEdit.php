<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tes Asprak</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
    <style>
        .mt40{
            margin-top: 40px;
        }
    </style>
</head>
<body>
    
<div class="container">
  
<div class="row">
    <div class="col-lg-12 mt40">
        <div class="pull-left">
            <h2>Edit Buku</h2>
        </div>
    </div>
</div>
     
     
<form action="<?php echo base_url('bukuController/store') ?>" method="POST" name="edit_buku">
   <input type="hidden" name="id" value="<?php echo $book->id ?>">
     <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <strong>Judul</strong>
                <input type="text" name="judul" class="form-control" value="<?php echo $book->title ?>" placeholder="Judul">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <strong>Pengarang</strong>
                <textarea class="form-control" col="4" name="pengarang"
                 placeholder="Pengarang"><?php echo $book->pengarang ?></textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <strong>Penerbit</strong>
                <textarea class="form-control" col="4" name="penerbit"
                 placeholder="Penerbit"><?php echo $book->penerbit?></textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <strong>Tahun</strong>
                <textarea class="form-control" col="4" name="tahun"
                 placeholder="Tahun"><?php echo $book->tahun ?></textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <strong>Status</strong>
                <textarea class="form-control" col="4" name="status"
                 placeholder="Status"><?php echo $book->status ?></textarea>
            </div>
        </div>
        <div class="col-md-12">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
     
 
</div>
     
</body>
</html>