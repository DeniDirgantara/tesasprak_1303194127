<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tes Asprak</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
    <style>
        .mt40{
            margin-top: 40px;
        }
    </style>
</head>
<body>
    
<div class="container">
    <div class="row mt40">
   <div class="col-md-10">
    <h2>Codeigniter Basic Crud Example</h2>
   </div>
   <div class="col-md-2">
    <a href="<?php echo base_url('bukuController/create/') ?>" class="btn btn-danger">Add Note</a>
   </div>
   <br><br>
 
    <table class="table table-bordered">
       <thead>
          <tr>
             <th>Id</th>
             <th>Judul</th>
             <th>Pengarang</th>
             <th>Penerbit</th>
             <th>Tahun</th>
             <th>Status</th>
             <td colspan="2">Action</td>
          </tr>
       </thead>
       <tbody>
          <?php if($books): ?>
          <?php foreach($books as $book): ?>
          <tr>
             <td><?php echo $note->id; ?></td>
             <td><?php echo $note->judul; ?></td>
             <td><?php echo $note->pengarang; ?></td>
             <td><?php echo $note->penerbit; ?></td>
             <td><?php echo $note->tahun; ?></td>
             <td><?php echo $note->status; ?></td>
             <td><a href="<?php echo base_url('bukuController/edit/'.$book->id) ?>" class="btn btn-primary">Edit</a></td>
                 <td>
                <form action="<?php echo base_url('bukuController/delete/'.$book->id) ?>" method="post">
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
          </tr>
         <?php endforeach; ?>
         <?php endif; ?>
       </tbody>
    </table>
    
</div>
 
</div>
     
</body>
</html>