<?php 
    class bukuController extends CI_Controller
    {
        public function __construct()
    {
        parent::__construct();
        $this->load->model('bukuModel');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
  
    public function index()
    {
        $data['buku'] = $this->notes_model->notes_list();
        $data['title'] = 'List Buku';
 
        $this->load->view('views/bukuViewGet', $data);
    }
  
    public function create()
    {
        $data['title'] = 'Masukkan Buku';
        $this->load->view('views/bukuViewInsert', $data);
    }
      
    public function edit($id)
    {
        $id = $this->uri->segment(3);
        $data = array();
 
        if (empty($id))
        { 
         show_404();
        }else{
          $data['buku'] = $this->notes_model->getOneBuku($id);
          $this->load->view('views/bukuViewEdit', $data);
        }
    }
    public function store()
    {
 
        $this->form_validation->set_rules('title', 'Title', 'required');
 
        $id = $this->input->post('id');
 
        if ($this->form_validation->run() === FALSE)
        {  
            if(empty($id)){
              redirect( base_url('bukuController/create') ); 
            }else{
             redirect( base_url('bukuController/edit/'.$id) ); 
            }
        }
        else
        {
            $data['buku'] = $this->notes_model->createOrUpdate();
            redirect( base_url('bukuController') ); 
        }
         
    }
     
     
    public function delete()
    {
        $id = $this->uri->segment(3);
         
        if (empty($id))
        {
            show_404();
        }
                 
        $notes = $this->notes_model->delete($id);
         
        redirect( base_url('buku') );        
    }
    }
?>