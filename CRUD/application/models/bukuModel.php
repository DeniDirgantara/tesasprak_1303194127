<?php
    class bukuModel extends CI_Model
    {
        public function __construct()
        {
            $this->load->database();
        }
         
        public function getBuku()
        {
            $query = $this->db->get('buku');
            return $query->result();
        }
         
        public function getOneBuku($id)
        {
            $query = $this->db->get_where('buku', array('id' => $id));
            return $query->row();
        }
         
        public function createOrUpdateBuku()
        {
            $this->load->helper('url');
            $id = $this->input->post('id');
     
            $data = array(
                'judul' => $this->input->post('judul'),
                'pengarang' => $this->input->post('pengarang'),
                'penerbit' => $this->input->post('penerbit'),
                'tahun' => $this->input->post('tahun'),
                'status' => $this->input->post('status')

            );
            if (empty($id)) {
                return $this->db->insert('buku', $data);
            } else {
                $this->db->where('id', $id);
                return $this->db->update('buku', $data);
            }
        }
         
        public function deleteBuku($id)
        {
            $this->db->where('id', $id);
            return $this->db->delete('buku');
        }
    }
?>