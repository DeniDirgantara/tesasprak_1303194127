let calDisplay = document.getElementById('calDisplay');

let semuaTombol = Array.from(document.getElementsByClassName('tombol'));

semuaTombol.map( tombol =>{
    tombol.addEventListener('click', (e) =>{
        switch(e.target.innerText){
            case 'C' : 
                calDisplay.innerText = '';
                break;
            case '=': 
                try {
                    calDisplay.innerText = eval(calDisplay.innerText);  
                } catch  {
                    calDisplay.innerText = "Error";
                }
                break;
            case '←': 
                try {
                    calDisplay.innerText = calDisplay.innerText.slice(0, -1);
                } catch (error) {}
                break;
            default:
                calDisplay.innerText += e.target.innerText;
        }
    });
});